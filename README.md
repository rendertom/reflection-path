![Reflection Path](/Reflection%20Path.gif)

# Reflection Path #
Script for After Effects v15 (CC2018) to calculate reflection/bounce point coordinates in a rectangle container, providing initial point position and direction angle.

Draws visual path representing reflection/bounce path. Reflection depth can be modified in effect controller.

### Installation: ###
Clone or download this repository and copy **Reflection Path.jsx** to Scripts folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking File -> Scripts -> **Reflection Path**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------